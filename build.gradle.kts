import nl.javadude.gradle.plugins.license.DownloadLicensesExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.owasp.dependencycheck.reporting.ReportGenerator

plugins {
    id("org.springframework.boot") version "2.3.0.M2"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    id("com.google.cloud.tools.jib") version "2.1.0"
    id("org.owasp.dependencycheck") version "5.3.0"
    id("com.github.hierynomus.license-report") version "0.15.0"
    kotlin("jvm") version "1.3.61"
    kotlin("plugin.spring") version "1.3.61"
    groovy
    jacoco
}

group = "io.tztz"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_12

repositories {
    mavenCentral()
    maven { url = uri("https://repo.spring.io/milestone") }
}

dependencyManagement {
    dependencies {
        dependencySet("org.spockframework:1.3-groovy-2.5") {
            entry("spock-core")
            entry("spock-spring")
        }
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-devtools")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
    implementation("org.springframework.boot:spring-boot-starter-security")

    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Prometheus metrics
    implementation("io.micrometer:micrometer-registry-prometheus")

    // --- Testing ---

    // Core
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
        exclude(group = "junit", module = "junit")
    }
    testImplementation("org.springframework.security:spring-security-test")

    // Spock testing
    testImplementation("org.spockframework:spock-core")
    testImplementation("org.spockframework:spock-spring")
}

tasks.withType<Test> {
    useJUnit()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "12"
    }
}

// ******
// 'bootRun' task sets "local" profile
// ******

tasks.bootRun {
    systemProperty("spring.profiles.active", "local")
}

// ******
// Jacoco
//
// https://docs.gradle.org/current/userguide/jacoco_plugin.html
// ******

jacoco {
}

val jacocoExcludes = listOf(
        "**/${project.property("mainClassName").toString()}.class",
        "**/${project.property("mainClassName").toString()}Kt.class"
)

tasks.jacocoTestReport {
    classDirectories.setFrom(
            fileTree("$buildDir/classes/kotlin/main").apply {
                exclude(jacocoExcludes)
            })

    reports {
        xml.isEnabled = true
        csv.isEnabled = false
    }
}

tasks.test {
    finalizedBy("jacocoTestReport")
}

// ******
// License-Report
//
// https://plugins.gradle.org/plugin/com.github.hierynomus.license-report
// ******

downloadLicenses {
    ext["apacheTwo"] = DownloadLicensesExtension.license(
            "The Apache License, Version 2.0",
            "http://opensource.org/licenses/Apache-2.0")
    aliases = mapOf(
            "apacheTwo" to listOf(
                    "ASF 2.0",
                    "Apache License, Version 2.0",
                    "Apache 2.0",
                    "Apache License 2.0",
                    "The Apache License, Version 2.0",
                    "Apache License v2.0",
                    "Apache Software License - Version 2.0",
                    "Apache License, version 2.0",
                    "Apache 2"
            )
    )
    includeProjectDependencies = true
}

// ******
// OWASP Dependency Check
//
// https://plugins.gradle.org/plugin/org.owasp.dependencycheck
// ******

dependencyCheck {
    analyzers(closureOf<org.owasp.dependencycheck.gradle.extension.AnalyzerExtension> {
        failBuildOnCVSS = 0F
        archiveEnabled = false
        assemblyEnabled = false
        autoconfEnabled = false
        bundleAuditEnabled = false
        centralEnabled = false
        cmakeEnabled = false
        cocoapodsEnabled = false
        composerEnabled = false
        nexusEnabled = false
        nugetconfEnabled = false
        nuspecEnabled = false
        opensslEnabled = false
        pyDistributionEnabled = false
        pyPackageEnabled = false
        rubygemsEnabled = false
        swiftEnabled = false
    })
    outputDirectory = "$buildDir/reports/owasp-dependency-check/"
    // suppressionFile = "owasp-dependency-check-suppressions.xml"
    failOnError = false
    format = ReportGenerator.Format.ALL
}

// ******
// jib
//
// https://plugins.gradle.org/plugin/com.google.cloud.tools.jib
// ******

jib {
    from {
        image = "openjdk:12-alpine"
    }
    container {
        ports = listOf("8080", "8080")
        creationTime = "USE_CURRENT_TIMESTAMP"
        labels = mapOf(
                "maintainer" to project.property("maintainer").toString()
        )
    }
}

tasks["jib"].dependsOn(tasks.build)
tasks["jibBuildTar"].dependsOn(tasks.build)
tasks["jibDockerBuild"].dependsOn(tasks.build)
