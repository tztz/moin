# Moin app

## Build Docker image

    ./gradlew jibDockerBuild  

## Run Docker container

    docker run -p 8080:8080 moin:0.0.1-SNAPSHOT  

## Run all backend tests

    ./gradlew clean test

### View test results and code coverage report

The test results and code coverage report are published to

    https://tztz.gitlab.io/moin/
 
Note: This is a public page.
