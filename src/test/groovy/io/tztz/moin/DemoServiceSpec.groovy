package io.tztz.moin

import spock.lang.Specification

class DemoServiceSpec extends Specification {

    Color blueColor = Mock()

    DemoService demoService = new DemoService(blueColor)

    def "should return correct color"() {
        when:
        def result = demoService.getColorMessage()

        then:
        1 * blueColor.getName() >> "completely blue!!!"
        result == 'Hi! The color is completely blue!!!'
    }
}
