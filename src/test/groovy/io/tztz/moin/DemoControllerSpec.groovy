package io.tztz.moin

import io.micrometer.core.instrument.Counter
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@WebMvcTest(controllers = [DemoController])
class DemoControllerSpec extends Specification {

    @SpringBean
    DemoService demoService = Mock()

    @SpringBean
    Counter greetingRequestCounter = Mock()

    @Autowired
    MockMvc mvc

    def "getGreeting endpoint should return correctly"() {
        given:
        1 * greetingRequestCounter.increment()

        expect:
        mvc
                .perform(get("/demo/greeting"))
                .andExpect(status().isOk())
                .andReturn()
                .response
                .contentAsString == "Hi!"
    }

    @WithMockUser(value = "foo-user")
    def "getColor endpoint should return correctly"() {
        given:
        1 * demoService.getColorMessage() >> "My Super Color!"

        expect:
        mvc
                .perform(get("/demo/color"))
                .andExpect(status().isOk())
                .andReturn()
                .response
                .contentAsString == "My Super Color!"
    }

    @WithMockUser(value = "foo-user")
    def "getYourDetails endpoint should return correctly"() {
        expect:
        mvc
                .perform(get("/demo/your-details"))
                .andExpect(status().isOk())
                .andReturn()
                .response
                .contentAsString == "Ahoi, foo-user!"
    }

    def "getDelayedGreetingToConsole endpoint should return correctly"() {
        given:
        1 * demoService.sayHiToConsoleLater(_)

        expect:
        mvc
                .perform(get("/demo/delayed-greeting"))
                .andExpect(status().isOk())
                .andReturn()
                .response
                .contentAsString.startsWith("Hi now from endpoint and later also to the console! Hash: ")
    }

    def "getColor endpoint should return 403 Forbidden if user not authenticated"() {
        when:
        def result = mvc.perform(get("/demo/color"))

        then:
        def response = result
                .andExpect(status().isForbidden())
                .andReturn()
                .response
        and:
        response.errorMessage == "Access Denied"
        response.contentAsString == ""
        and:
        0 * demoService.getColorMessage()
    }

    def "getYourDetails endpoint should return 403 Forbidden if user not authenticated"() {
        when:
        def result = mvc.perform(get("/demo/your-details"))

        then:
        def response = result
                .andExpect(status().isForbidden())
                .andReturn()
                .response
        and:
        response.errorMessage == "Access Denied"
        response.contentAsString == ""
    }
}
