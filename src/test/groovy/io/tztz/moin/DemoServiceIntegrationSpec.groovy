package io.tztz.moin

import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class DemoServiceIntegrationSpec extends Specification {

    @SpringBean(name = "blue")
    Color blueColor = Mock()

    @Autowired
    DemoService demoService

    def "getColorMessage returns correct color message"() {
        when:
        def result = demoService.getColorMessage()

        then:
        1 * blueColor.getName() >> "completely blue!!!"
        and:
        result == 'Hi! The color is completely blue!!!'
    }
}
