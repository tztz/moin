package io.tztz.moin

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.*

@Configuration
class ColorsConfig {

    val id: UUID = UUID.randomUUID()

    @Bean
    fun blue(): Color = object : Color {
        override fun getName() = "Blue - $id"
    }

    @Bean
    fun red(): Color = object : Color {
        override fun getName() = "Red - $id"
    }
}
