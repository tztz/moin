package io.tztz.moin

import io.micrometer.core.instrument.Counter
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Tag
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MetricsConfig(private val meterRegistry: MeterRegistry) {

    @Bean
    fun greetingRequestCounter(): Counter =
            meterRegistry.counter("greetingRequestCounter", listOf(Tag.of("foo", "bar")))
}
