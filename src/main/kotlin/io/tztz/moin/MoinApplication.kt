package io.tztz.moin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableAsync

@SpringBootApplication
@EnableAsync
class MoinApplication

fun main(args: Array<String>) {
    runApplication<MoinApplication>(*args)
}
