package io.tztz.moin

interface Color {
    fun getName(): String
}
