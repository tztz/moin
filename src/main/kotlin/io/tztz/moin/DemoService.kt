package io.tztz.moin

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import java.time.Duration
import java.util.*

@Service
class DemoService(@Qualifier("blue") private val color: Color) {

    val logger: Logger = LoggerFactory.getLogger(DemoService::class.java)

    fun getColorMessage(): String {
        logger.debug("getColor service method called")
        return "Hi! The color is ${color.getName()}"
    }

    @Async
    fun sayHiToConsoleLater(hash: UUID) {
        Thread.sleep(Duration.ofSeconds(5).toMillis())
        logger.info("Hi now from service! Hash: $hash")
    }
}
