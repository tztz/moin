package io.tztz.moin

import io.micrometer.core.annotation.Timed
import io.micrometer.core.instrument.Counter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal
import java.util.*

@Timed
@RestController
@RequestMapping("/demo")
class DemoController(
        private val demoService: DemoService,
        @Qualifier("greetingRequestCounter") private val greetingRequestCounter: Counter
) {
    val logger: Logger = LoggerFactory.getLogger(DemoController::class.java)

    @GetMapping("greeting")
    fun getGreeting(): String {
        logger.debug("getGreeting endpoint called")
        greetingRequestCounter.increment()
        return "Hi!"
    }

    @GetMapping("color")
    fun getColor(): String {
        logger.debug("getColor endpoint called")
        return demoService.getColorMessage()
    }

    @GetMapping("your-details")
    fun getYourDetails(principal: Principal): String {
        logger.debug("getYourDetails endpoint called")
        return "Ahoi, ${principal.name}!"
    }

    @GetMapping("delayed-greeting")
    fun getDelayedGreetingToConsole(): String {
        val hash = UUID.randomUUID()
        logger.debug("getDelayedGreetingToConsole endpoint called - Hash: $hash")
        demoService.sayHiToConsoleLater(hash)
        return "Hi now from endpoint and later also to the console! Hash: $hash"
    }

    @GetMapping("failure")
    fun failOnPurpose(): Nothing = throw RuntimeException("FAILED ON PURPOSE!")
}
