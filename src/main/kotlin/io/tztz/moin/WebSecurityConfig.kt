package io.tztz.moin

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableWebSecurity
class WebSecurityConfig : WebSecurityConfigurerAdapter() {
    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
                .csrf().disable()
                .authorizeRequests { a ->
                    a
                            .antMatchers("/", "/error").permitAll()
                            .antMatchers("/demo/greeting").permitAll()
                            .antMatchers("/demo/delayed-greeting").permitAll()
                            .antMatchers("/demo/failure").permitAll()
                            .antMatchers("/actuator/info", "/actuator/health").permitAll()
                            .antMatchers("/actuator/metrics/**").permitAll()
                            .antMatchers("/actuator/prometheus/**").permitAll()
                            .anyRequest().authenticated()
                }
                // This causes a redirect to the login page which we don't want:
                // .oauth2Login()

        // TODO: do not expose all metrics
    }
}
